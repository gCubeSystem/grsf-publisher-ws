package org.gcube.data_catalogue.grsf_publish_ws;

import java.io.File;
import java.net.URL;

import org.gcube.data_catalogue.grsf_publish_ws.json.input.record.StockRecord;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TestJson {
	
	private static final Logger logger = LoggerFactory.getLogger(Test.class);
	
	public File getResourcesDirectory() throws Exception {
		URL logbackFileURL = TestJson.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return resourcesDirectory;
	}
	
	@Test
	public void testJsonDeserialization() throws Exception {
		File jsonQueryFile = new File(getResourcesDirectory(), "70ae6895-7d3d-4f4a-86f9-bcb17d41bff6.json");
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(jsonQueryFile);
		logger.debug("{}", jsonNode);
		StockRecord sr = objectMapper.readValue(jsonQueryFile, StockRecord.class);
		logger.debug("{}", sr);
	}
	
}
