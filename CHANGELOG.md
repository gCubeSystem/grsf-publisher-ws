# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.13.3]

- Adding support for "FAO SDG 14.4.1 Questionnaire" source [#23670] 


## [v1.13.2]

- Migrated request to social-service-client [#23679]
- Added "Assessment Methods" as Group [#23409]


## [v1.13.1]

- Aligned code and wiki to the new requirements [#23167]
- Changed group assign strategy [#23211] [#23215]
- Tag are added also to legacy records [#23216]
- Fixed code which generated groups id from name [#23215]


## [v1.13.0]

### Added

- Added support to not include in records time dependant metadata [#21995]

### Changed

- Switched dependency management to gcube-bom 2.0.0
- Migrated code to storagehub [#21432]


## [v1.12.0] - 2020-06-19

### Added

**Features**

- [#19166] Added support for GRSF_pre VRE with the behaviour of GRSF Admin

## [v1.11.0] - 2020-03-30

### Changed

- [#18293] Traceability flag is assigned only to Fishery records

## [v1.10.0] - 2019-11-11

### Changed

- [#17965] Tags longer than 100 characters are truncated instead of skipped

## [v1.9.0] - 2019-05-27

### Changed

- [#13347] refers_to can be null while publishing legacy records

- [#12421] Removed the non-ascii clean from extra fields

- [#12421] Properly supporting UTF-8 characters

- [#16395] Title is updated according to Stock/Fishery Name

## [v1.8.0] - 2019-02-26

### Changed

- [#12861] The sources in GRSF VRE are calculated using 'database_sources' field

## [v1.7.0] - 2018-10-10

### Changed

- [#12510] Fixed pom to exclude libraries already provided by the container

## [v1.6.0] - 2018-07-18

### Changed

- [#11464] Added biomass timeseries support to stock data
- [#11749] Added 'With Similarities'-'No Similarities' tag to GRSF Records
- [#11748] Added Tag for 'Fishing Gears' and 'Flag State' fields
- [#11766] Added 'Connected'-'Not Connected' tag to GRSF Records
- [#11767] Added group for SDG flag
- [#11811] Added citation field
- [#11832] Added sub-groups support for available time series related to GRSF Type "Assessment Unit"
- [#11967] Added Biomass group
- [#11968] Changed 'State and trend of Marine Resource' to 'State and Trend'
- [#11969] Changed 'Scientific advice' to 'Scientific Advice'

## [v1.5.0] - 2017-01-10

### Changed

- Model enhancements

## [v1.4.0] - 2017-11-02

### Changed

- Some fixes and improvements: added a common library between service and management widget

## [v1.3.0] - 2017-08-01

### Changed

- Model upgrade

## [v1.2.0] - 2017-07-01

### Changed

- [#8719] Model changed

## [v1.1.2] - 2017-05-15

- [#8719] Updates for ticket
- Minor fixes

## [v1.1.1] - 2017-04-02

- Minor fixes

## [v1.1.0] - 2017-02-28

- Model update

## [v1.0.1] - 2017-02-10

- Minor fixes

## [v1.0.0] - 2016-12-10

- First Release
